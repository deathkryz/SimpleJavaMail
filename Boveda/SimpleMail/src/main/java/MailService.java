import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

/**
 * A singleton class for sending mail messages.
 *
 * @author tcolburn
 */
public class MailService {

    /**
     * Sends a subject and message to a recipient
     *
     * @param recipient Internet address of the recipient
     * @param subject the subject of the message
     * @param message the message
     * @param mimeBodyPart
     * @throws MessagingException
     *
     *
     */
    public static void sendMessage(String recipient, String subject, String message, MimeBodyPart[] mimeBodyPart) throws MessagingException, Exception {

        if (theService == null) {
            theService = new MailService();
        }

        MimeMessage mimeMessage = new MimeMessage(mailSession);

        mimeMessage.setFrom(new InternetAddress(InvitacionesTW.FROM));
        mimeMessage.setSender(new InternetAddress(InvitacionesTW.FROM));
        UUID idOne = UUID.randomUUID();
        String encodedSubject = new String((subject + " #" + idOne.toString()).getBytes());

        mimeMessage.setSubject(encodedSubject);
        String mensaje = message;
        String encodedBody = new String(mensaje.getBytes());

        mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        InternetAddress[] myCcList = InternetAddress.parse(InvitacionesTW.COPYTO);
        //InternetAddress[] myCcListA = InternetAddress.parse("servicio.cliente@montenegroeditores.net");

        mimeMessage.addRecipients(Message.RecipientType.BCC, myCcList);
        // mimeMessage.addRecipients(Message.RecipientType.BCC, myCcListA);

        MimeMultipart content = new MimeMultipart();
        MimeBodyPart texto = new MimeBodyPart();
        texto.setText(encodedBody, "utf-8", "html");

        content.addBodyPart(texto);
        if (mimeBodyPart != null) {
            for (int i = 0; i < mimeBodyPart.length; i++) {
                MimeBodyPart mimeBodyPart1 = mimeBodyPart[i];
                content.addBodyPart(mimeBodyPart1);

            }
        }

        mimeMessage.setContent(content, "text/html; charset=utf-8");

        Transport transport = mailSession.getTransport("smtp");
        transport.connect(InvitacionesTW.HOST, Integer.parseInt(InvitacionesTW.PORT), InvitacionesTW.USER, InvitacionesTW.PASSWORD);

        transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        transport.close();

    }

    public static void sendMessageiWithBCC(String recipient, List<String> bccList, String subject, String message, ByteArrayOutputStream attachment, String remitente, String password) throws MessagingException, Exception {
        if (theService == null) {
            theService = new MailService();
        }
        MimeMessage mimeMessage = new MimeMessage(mailSession);
        mimeMessage.setFrom(new InternetAddress(remitente));
        mimeMessage.setSender(new InternetAddress(remitente));
        mimeMessage.setSubject(subject);

        String mensaje = "<center>\n"
                + "        <img width=\"470\"  height=\"180\" src=\"cid:image\">"
                + "    </center>\n"
                + message + "\n\n\n Mensaje enviado del Sistema Boveda.";

        MimeBodyPart messageBodyPart = new MimeBodyPart();

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);

        DataSource source = new FileDataSource("");
        mimeMessage.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName("");
        multipart.addBodyPart(messageBodyPart);

        mimeMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
        for (String bccMail : bccList) {
            mimeMessage.addRecipient(Message.RecipientType.BCC, new InternetAddress(bccMail));
        }

        MimeMultipart mp = new MimeMultipart("related");
        BodyPart mbp = new MimeBodyPart();
        System.out.println("--------------MENSAJE/n" + mensaje);
        mbp.setContent(mensaje, "text/html");

        mp.addBodyPart(mbp);

        mbp.setHeader("Content-ID", "<image>");

        mp.addBodyPart(mbp);

        MimeBodyPart download
                = download = new MimeBodyPart();
        //construct the pdf body part

        byte[] bytes = attachment.toByteArray();

        DataSource dataSource = new ByteArrayDataSource(bytes, "application/pdf");
        download.setHeader("Content-Transfer-Encoding", "base64");
        download.setDataHandler(new DataHandler(dataSource));
        download.setFileName("factura.pdf");
        mp.addBodyPart(download);

        // put everything together
        mimeMessage.setContent(mp);
        Transport transport = mailSession.getTransport("smtp");
        transport.connect(InvitacionesTW.HOST, Integer.parseInt(InvitacionesTW.PORT), InvitacionesTW.USER, InvitacionesTW.PASSWORD);

        transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
        transport.close();
    }

    private MailService() {

        Properties props;
        props = System.getProperties();
        props.put("mail.smtp.port", InvitacionesTW.PORT);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", InvitacionesTW.STARTTLS);
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtps.host", InvitacionesTW.HOST);
        props.put("mail.smtps.auth", "true");
        props.put("mail.smtp.from", InvitacionesTW.FROM);

        mailSession = Session.getDefaultInstance(props);
        mailSession.setDebug(true);

    }

    private static MailService theService = null;

    private static Session mailSession;

    /*
    private static Session mailSession;
    private static final String HOST = "nerdsystemscom.ipage.com";
    private static final int PORT = 587;
    private static final String USER = "indicadores@pharah.mx";     // Must be valid user in d.umn.edu domain, e.g. "smit0012"
    private static final String PASSWORD = "Pharah123"; // Must be valid password for smit0012
    private static final String FROM = "indicadores@pharah.mx";     // Full info for user, e.g. "Fred Smith <smit0012@d.umn.edu>"
     */
}
